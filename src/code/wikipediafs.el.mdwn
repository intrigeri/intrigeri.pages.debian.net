[[!meta title="wikipediafs.el"]]

`wikipediafs.el` enhances the wikipedia Emacs mode when using
WikipediaFS.

[WikipediaFS](https://en.wikipedia.org/wiki/WikipediaFS) is a FUSE filesystem
that allows viewing and editing Wikipedia articles as if they were
real files. It actually works for Mediawiki in general.

As of the time of this writing, the main `wikipediafs.el` feature
is the ability to add a changes summary, i.e. commit message, when
saving changes to a buffer.

`wikipediafs.el` is developed using Git:

    git clone https://gaffer.boum.org/intrigeri/git/wikipediafs-el.git

A [page on the EmacsWiki](https://www.emacswiki.org/emacs/WikipediaFs) is dedicated to `wikipediafs.el`.

I am not using `wikipediafs.el` anymore but might need it again at
some point.
