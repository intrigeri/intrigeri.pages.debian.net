[[!meta title="patafisque"]]

If a target user has no running session, patafisque umounts the
volumes managed by pam_mount for her, after having optionally killed
part or all of the remaining processes she owns.

One typically runs this program as a cronjob to ensure encrypted
`$HOME` directories are umounted once their owner has logged-out of
the system.

patafisque uses `ConsoleKit`.

patafisque is available in Git:

    git clone https://gaffer.boum.org/intrigeri/git/patafisque.git
