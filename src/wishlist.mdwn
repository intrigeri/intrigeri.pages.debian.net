[[!toc levels=2]]

# Books

## Other

- _What Did You Say? The Art of Giving and Receiving Feedback_, by Charles N. Seashore
  - [Chasse aux Livres](https://www.chasse-aux-livres.fr/prix/0965043002/)
  - [amazon.com](https://www.amazon.com/dp/0965043002)

- _Le grand traité du jardin punk_, by Éric Lenoir
  - [Terre Vivante (éditeur)](https://www.terrevivante.org/boutique/livres/livres-jardin-bio/amenagements-jardin/le-grand-traite-du-jardin-punk/)
  - [Chasse aux livres](https://www.chasse-aux-livres.fr/prix/2360985655/)

## Woodworking

- _American Furniture of the 18th Century: History, Technique & Structure_,
  by Jeffrey P. Greene
  - [Chasse aux Livres](https://www.chasse-aux-livres.fr/prix/1561581046/)
  - [amazon.com](https://www.amazon.com/dp/1561581046)

- _Joinery, Joists and Gender: A History of Woodworking for the 21st Century_,
  by Deirdre Visser
  - [Review by Nancy Hiller](https://blog.lostartpress.com/2022/05/04/book-report-joinery-joists-and-gender-by-deirdre-visser/)
  - [AbeBooks.fr](https://www.abebooks.fr/products/isbn/9780367363413)
  - [Chasse aux Livres](https://www.chasse-aux-livres.fr/prix/0367363410/)
  - [amazon.fr](https://www.amazon.fr/dp/0367363410)

- _The Solution at Hand_, by Robert Wearing
  - [AbeBooks.fr](https://www.abebooks.fr/products/isbn/9781733391603)
  - [Chasse aux Livres](https://www.chasse-aux-livres.fr/prix/O00H7ULH6Q/)
  - [Classic Hand Tools](https://www.classichandtools.com/acatalog/The-Solution-at-Hand-LAP_TSaH.html)
  - [Dictum](https://www.dictum.com/fr/assemblage-de-meubles-travail-du-bois-hc/the-solution-at-hand-714410)
  - [Rubank Verktygs AB](https://www.hyvlar.se/en/the-solution-at-hand)
  - [amazon.com](https://www.amazon.com/dp/1733391606/)
